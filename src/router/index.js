import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: "/login"
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ '../views/login.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ '../views/home.vue'),
      children: [
        {
          path: '/',
          redirect:"subject"
        },
        {
          path: 'subject',
          name: 'subject',
          component: () => import(/* webpackChunkName: "home" */ '../components/subject/subject.vue')
        },
        {
          path: 'user',
          name: 'user',
          component: () => import(/* webpackChunkName: "home" */ '../components/user/user.vue')
        },{
          path: 'title',
          name: 'title',
          component: () => import(/* webpackChunkName: "home" */ '../components/title/title.vue')
        },{
          path: 'options',
          name: 'options',
          component: () => import(/* webpackChunkName: "home" */ '../components/options/options.vue')
        },
        {
          path: 'message',
          name: 'message',
          component: () => import(/* webpackChunkName: "home" */ '../components/message/message.vue')
        }
      ]
    }
  ]
})

export default router
