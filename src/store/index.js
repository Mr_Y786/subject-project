import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
//设置初始化的值
function initiaState(){
  return {
    token:localStorage.getItem("token") || "",
    username:localStorage.getItem("username") || "",
    editGoodsId:"",
  }
}

export default new Vuex.Store({
  state: initiaState(),
  mutations: {
    setToken(state,data){
      state.token=data;
      localStorage.setItem("token",data)
    },
    setUsername(state,data){
      console.log(data)
      state.username=data;
      localStorage.setItem("username",data)
    },
    //重置state(用于退出登陆效果)
    //  语法: Object.assign(target, …sources) target: 目标对象,sources: 源对象
    // 用于将所有可枚举属性的值从一个或多个源对象复制到目标对象。它将返回目标对象。
    resetState(state){
      Object.assign(state,initiaState())
    }
  },
  actions: {
  },
  modules: {
  }
})
