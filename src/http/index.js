import axios from "axios"
import { Message } from 'element-ui';
import store from "../store"
import router from "../router"
const baseURL="http://47.111.119.98:8081";
const instance = axios.create({
    baseURL: baseURL
})
// 添加请求拦截器
instance.interceptors.request.use(config => {
    // 在发送请求之前做些什么
    if (store.state.token) {
        config.headers.token = store.state.token
    }
    return config;
}, error => {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(response => {
    // 对响应数据做点什么
    if (response.data.msg == "请登录") {
        router.push("/login")
    }
    return response;
}, error => {
    // 对响应错误做点什么
    
    return Promise.reject(error);
});
export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        instance({
            url,
            method,
            data,
            params
        }).then(res => {
            
            if ((res.status >= 200 && res.status < 300) || res.status === 304) {
                if (res.data.code >= 200 && res.data.code< 300 || res.data.code===304) {
                    resolve(res.data)
                } else {
                    Message({
                        showClose: true,
                        message: res.data.msg,
                        type: 'error'
                    });
                    reject(res)
                }
            } else {
                Message({
                    showClose: true,
                    message: res.statusText,
                    type: "error"
                })
                reject(res)
            }
        }).catch(err => {
            Message({
                showClose: true,
                message: err.message,
                type: "error"
            })
            reject(err)
        })
    })
}

export const uploadURL= baseURL+"/common/upload"